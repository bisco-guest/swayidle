swayidle (1.8.0-1) unstable; urgency=medium

  * New upstream version.

 -- Birger Schacht <birger@debian.org>  Mon, 05 Dec 2022 15:18:07 +0100

swayidle (1.7.1-3) unstable; urgency=medium

  * Fix d/watch file by switching to gitmode
  * Bump standards-version to 4.6.1.0 (no changes required)

 -- Birger Schacht <birger@debian.org>  Fri, 14 Oct 2022 21:22:53 +0200

swayidle (1.7.1-2) unstable; urgency=medium

  * d/rules: Set -Wno-error=format (Closes: #1004092)
  * Drop filenamemangle option from d/watch

 -- Birger Schacht <birger@debian.org>  Wed, 06 Apr 2022 16:37:30 +0200

swayidle (1.7.1-1) unstable; urgency=medium

  * New upstream release
  * Update copyright years in d/copyright
  * Bump standards-version to 4.6.0.1 (no changes required)

 -- Birger Schacht <birger@debian.org>  Thu, 20 Jan 2022 15:28:04 +0100

swayidle (1.7-1) unstable; urgency=medium

  * New upstream release
  * Remove constraints unnecessary since stretch:
    + Build-Depends: Drop versioned constraint on meson and wayland-protocols.
  * Bump debhelper-compat to 13
  * Bump standards-version to 4.5.1 (no changes required)
  * Update uploader address
  * Set field Upstream-Name in debian/copyright.
  * Set upstream metadata fields: Bug-Database, Bug-Submit and remove obsolete
    field Name
  * Drop patch to set version in meson file
  * Fix trailing whitespace in d/changelog
  * Fix regex in d/watch

 -- Birger Schacht <birger@debian.org>  Mon, 30 Aug 2021 19:12:22 +0200

swayidle (1.6-2) unstable; urgency=medium

  * Source only upload as-is.

 -- Birger Schacht <birger@rantanplan.org>  Fri, 07 Feb 2020 11:36:04 +0100

swayidle (1.6-1) unstable; urgency=medium

  * New upstream release
  * Add d/gbp.conf
  * d/patches: Add 0002-Fix-the-version-in-the-build-file.patch
  * d/control: Bump standards version to 4.5.0 (no changes required)

 -- Birger Schacht <birger@rantanplan.org>  Tue, 04 Feb 2020 18:41:47 +0100

swayidle (1.5-1) unstable; urgency=medium

  * New upstream release
   + Drop patch to fix version in buildfile
  * d/control:
   + Set maintainer field to new team address and add nicoo to
     uploaders
   + Point Vcs-* fields to the new repository in the swaywm-team
     salsa group
   + Bump standards version to 4.4.0

 -- Birger Schacht <birger@rantanplan.org>  Mon, 22 Jul 2019 21:58:30 +0200

swayidle (1.3-1) unstable; urgency=medium

  * New upstream release
  * Add patch to fix the version in the meson buildfile
  * d/control: Set versions for build dependencies as specified
    by upstream build file
  * d/gitlab-ci.yml: Add a basic gitlab ci configuration

 -- Birger Schacht <birger@rantanplan.org>  Sun, 05 May 2019 11:27:15 +0200

swayidle (1.2-2) unstable; urgency=medium

  * Added libsystemd-dev to d/control (Closes: #922464)
  * Add patch to install zsh completion into correct directory

 -- Birger Schacht <birger@rantanplan.org>  Sun, 03 Mar 2019 18:21:42 +0100

swayidle (1.2-1) unstable; urgency=medium

  * Initial packaging (Closes: 921496)

 -- Birger Schacht <birger@rantanplan.org>  Sat, 09 Feb 2019 09:52:49 +0100
